
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Main {
	
	List<RoutePlanner> allRoutes = new ArrayList<>();
	
	void showAllRoutes() {
		
			try {
				BufferedReader br = new BufferedReader(new FileReader("D:\\routes.csv"));
				String line;
				int i=0;
				while((line = br.readLine())!=null) {
					if(i==0) {
						i++;
						continue;
					}
					
					String values[] = line.split(",");
					allRoutes.add(new RoutePlanner(values[0], values[1], values[2], values[3], values[4]));
				}
				
				br.close();
				System.out.println("---------- Showing All Routes Details ----------");
				System.out.println("FromCity    ToCity   Distance   Time   Airfare");
				for(RoutePlanner it : allRoutes) {
					System.out.println(it.getFromCity() + " " + it.getToCity() + " " 
				+ it.getDistance() + " " + it.getTime() + " " + it.getAirFare());
				}
			} 
			
			catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
	}
	
	void showDirectFlights(List<RoutePlanner> allRoutes, String fromCity) {
		boolean flag = false;
		System.out.println("\n----Showing Direct flights from " + fromCity + "----");
		System.out.println("FromCity  ToCity  Distance  Time  Airfare");
		for(RoutePlanner it : allRoutes) {
			if(it.getFromCity().equalsIgnoreCase(fromCity)) {
				flag = true;
				System.out.println(it.getFromCity() + " " + it.getToCity() + " " 
						+ it.getDistance() + " " + it.getTime() + " " + it.getAirFare());
			}
		}
		if(flag == false) {
			System.out.println("We are sorry. At this time, we don't have information on flights originating from " + fromCity);
		}
	}
	
	//Sorting
	void sortDirectFlights(List<RoutePlanner> allRoutes, String fromCity) {
		Collections.sort(allRoutes, (o1, o2) -> (o1.getToCity().compareTo(o2.getToCity())));
		showDirectFlights(allRoutes, fromCity);
	}
	
	//Finding All Connections between two cities
	List<RoutePlanner> path = new ArrayList<>();
	boolean foundPaths = false;
	boolean isFirstTime = true;
	
	void showAllConnections(List<RoutePlanner> allRoutes, String fromCity, String toCity) {
		
		if(isFirstTime) {
			for(RoutePlanner it : allRoutes) {
				if(it.getFromCity().equalsIgnoreCase(fromCity) && it.getToCity().equalsIgnoreCase(toCity)) {
					System.out.println(it.getFromCity() + " " + it.getToCity() + " " 
							+ it.getDistance() + " " + it.getTime() + " " + it.getAirFare());
					foundPaths = true;			
				}
			}
			isFirstTime = false;
		}
		
		for(RoutePlanner it : allRoutes) {
			if(it.getFromCity().equalsIgnoreCase(fromCity) && it.getToCity().equalsIgnoreCase(toCity)) {
				if(path.size()!=0) {
					for(RoutePlanner itr: path) {
						System.out.println(itr.getFromCity() + " " + itr.getToCity() + " " 
								+ itr.getDistance() + " " + itr.getTime() + " " + itr.getAirFare());
					}
					
					System.out.println(it.getFromCity() + " " + it.getToCity() + " " 
							+ it.getDistance() + " " + it.getTime() + " " + it.getAirFare());
					foundPaths = true;
					path.clear();
				}		
			}
			
			else if(it.getFromCity().equalsIgnoreCase(fromCity)) {
				path.add(it);
				showAllConnections(allRoutes, it.getToCity(), toCity);
			}
		}
			
		if(foundPaths == false)
			System.out.println("there is no information about route");
	}
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Main m = new Main();
		Scanner sc = new Scanner(System.in);
		m.showAllRoutes();
	
		System.out.println("\nQueries : " +"\n1.Show direct flights from a city"+ 
				"\n2.Show sorted direct flights from a city" + "\n3.Show All Connections between two cities" + "\nEnter the Query no. among (1,2,3):");
		
		int choice=0;
		try{
			choice = sc.nextInt(); sc.nextLine();
		}
		catch(Exception e){			
		}
		switch(choice) {
			case 1:
				System.out.println("Enter the source city:");
				String src = sc.nextLine();
				m.showDirectFlights(m.allRoutes, src);
				break;
			case 2:
				System.out.println("Enter the source city:");
				src = sc.nextLine();
				m.sortDirectFlights(m.allRoutes, src);
				break;
			case 3:
				System.out.println("Enter the source city:");
				src = sc.nextLine();
				System.out.println("Enter the destination city:");
				String des = sc.nextLine();
				System.out.println("FromCity ToCity  Distance  Time  Airfare");
				m.showAllConnections(m.allRoutes, src, des);
				break;
			default:
				System.out.println("Wrong Query no. entered");
				break;			
		}		
		sc.close();
	}

}

